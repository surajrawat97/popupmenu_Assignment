package com.example.ssr.popupmenuassignment;

import android.app.Dialog;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import static com.example.ssr.popupmenuassignment.R.id.text;

public class MainActivity extends AppCompatActivity {
Button b,b1,b2,b3,button;
    EditText et1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        b= (Button) findViewById(R.id.button);



        b.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                PopupMenu p=new PopupMenu(MainActivity.this,b);
                p.getMenuInflater().inflate(R.menu.popupmenu,p.getMenu());

                p.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item)
                    {

                        switch (item.getItemId())
                        {
                            case R.id.item1:
                            final Dialog dialog=new Dialog(MainActivity.this);
                            dialog.setContentView(R.layout.color);
                            dialog.setCancelable(true);

                            b1= (Button) dialog.findViewById(R.id.Green);
                            b2= (Button) dialog.findViewById(R.id.Blue);
                            b3= (Button) dialog.findViewById(R.id.Red);
                            b1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v)
                                {
                                b.setBackgroundColor(Color.GREEN);
                                    dialog.dismiss();
                                }
                            });

                            b2.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v)
                                {
                                b.setBackgroundColor(Color.BLUE);
                                    dialog.dismiss();
                                }
                            });

                            b3.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v)
                                {
                                b.setBackgroundColor(Color.RED);
                                    dialog.dismiss();
                                }
                            });
dialog.show();

break;

                            case R.id.item2:

                            final Dialog dialog1=new Dialog(MainActivity.this);
                            dialog1.setContentView(R.layout.rename);
                            dialog1.setCancelable(true);


                            et1= (EditText) dialog1.findViewById(R.id.et1);
                            button= (Button) dialog1.findViewById(R.id.button);
                            et1.setText(b.getText().toString());

button.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v)
    {
    b.setText(et1.getText().toString());dialog1.dismiss();
    }
});
                            dialog1.show();

break;

                        }
                        return false;
                    }
                });

                p.show();
            }
        });
    }



}
